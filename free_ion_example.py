#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import numpy.linalg as LA
import pandas

from readcross import * 

# Copied wholesale with minor compatibility tweaks from Jevon's
# free_ion_example.py to see if we get consistent answers.
#
# For original see:
# https://github.com/jevonlongdell/dieke/tree/master/examples 
#
# Example of free ion calculation using dieke
#
# This makes Figure 3.1 in Mike Reids notes which shows
# the energy levesl of Pr3+ as the strength of the spin
# orbit interaction is increased from zero to about 150%
# of it's actual value.
#
# www2.phys.canterbury.ac.nz/~mfr24/electronicstructure/00electronic.pdf


def readLaF3params(nf):
    pd = pandas.read_excel('carnall89params.xls',skiprows=2).set_index('param')
    RareEarths =['La','Ce', 'Pr','Nd', 'Pm', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy', 'Ho',
       'Er', 'Tm', 'Yb']
    p = {}
    re = RareEarths[nf]
    for k in pd[re].keys():
        if not np.isnan(pd[re][k]):
            p[k]=pd[re][k]
    if p.has_key('M0'):
        p['M2']=0.56*p['M0']
        p['M4']=0.31*p['M0']
    if p.has_key('P2'):
        p['P4']=0.5*p['P2']
        p['P6']=0.1*p['P2']
    return p


#nf = input('Enter number of f electrons: ')
nf=2
(LS_matel, sl_l) = read_cross(nf)
(LSJ_matel, slj_l) = sljcalc(LS_matel, sl_l)

# Convert to dense matrices from sparse upper tirangular.
matel = {}
for k in LSJ_matel:
    m = LSJ_matel[k].toarray()
    matel[k] = m + m.transpose() - np.diag(np.diag(m))

cfparams = readLaF3params(nf)

# Get the spin orbit coupling parameter
zeta0 = cfparams['ZETA']

# Make a list of spin orbit parameters for the plot
zetavals = np.linspace(0, 1.5*zeta0, 100)

numLSJ = len(matel['F2'])
nrglevels = np.zeros([len(zetavals), numLSJ])
H0 = np.zeros([numLSJ,numLSJ])

for k in cfparams.keys():
    print("using parameter ", k)
    if k in matel:
        H0 = H0+cfparams[k]*matel[k]
(evals,evects) = np.linalg.eig(H0)
E0 = np.min(evals)

cfparams2 = cfparams
for i, zeta in enumerate(zetavals):
    cfparams2['ZETA'] = zeta
    H = np.zeros([numLSJ, numLSJ])
    for k in cfparams2.keys():
        if k in matel:
            H = H + cfparams2[k]*matel[k]
    (evals, evects) = np.linalg.eig(H)
    nrglevels[i, :] = np.sort(evals)-E0


plt.axis([0, max(zetavals), -2000, 25000])
plt.plot(zetavals, nrglevels, [zeta0, zeta0], [-10000, +50000])
plt.show()          
       
       
   
   
