#!/usr/bin/env python
import numpy as np
from scipy.sparse import lil_matrix

from njsymbols import *

def spec2int(c):
    "Convert a spectroscopic notation L quantum number to its numerical value."
    try:
        return 'SPDFGHIKLMNOQRTUV'.index(c)
    except ValueError:
       raise ValueError("Unsupported L quantum number: %s."%c)

def int2spec(i):
    "Convert an L quantum number numerical value to spectroscopic notation."
    if i < 0: 
        raise ValueError("Unsupported L quantum number: %i."%c)
    try:
        return 'SPDFGHIKLMNOQRTUV'[i]
    except IndexError:
        raise ValueError("Unsupported L quantum number: %i."%c)

class SL_label(object):
    def __init__(self, label):
        self.S = int(label[0])-1
        self.L = spec2int(label[1])*2

        if len(label) == 3:
            self.X = int(label[2])
        else:
            self.X = 0

        self.label = '|%-3s>'%label

class SLJ_label(object):
    def __init__(self, sl_lab, J, whichSL):
        self.S = sl_lab.S
        self.L = sl_lab.L
        self.X = sl_lab.X
        self.J = J
        self.whichSL = whichSL
        
        if J%2:
            jlab = J
        else:
            jlab = J/2
        self.label = '|%-3s%+2s>'%(sl_lab.label[1:4], jlab)

def read_cross(n):
    """
    Read the Crosswhite files, and return LS coupled matrix elements. 

    Parameters
    ----------
    n : int
        The number of electrons.

    Returns
    -------
    (LS_matel, ls_l) : tuple
        The matrix elements and a list of SL_label objects.
    """

    # NOTE: we use scipy linked list matrix throughout here even though later
    # we'll need to convert to compressed sparse row for input to pycf;
    # lil_matrix is much more efficient at incrementally adding elements than
    # CSR. 
    input_file='fn_crosswhite/f%icross'%(int(n))
    f = open(input_file,'r')

    # Read first line
    line = f.readline().split()
    nelectrons = int(line[1])
    nsl = int(line[2])      # number of LS terms
    
    # Read LS term labels
    LS_terms = f.readline().split()
    while (len(LS_terms) != nsl):
        line = f.readline()
        line = line.split()
        map(LS_terms.append,line)
    
    # Next are coefficients of fractional parentage; ignore for now and read
    # until we hit END
    while ('END' not in line):
        line = f.readline()
    
    # Storage for LS coupled matrix elements
    LS_matel = {}
    for i in range(7):
        LS_matel['U%i'%i] = lil_matrix((nsl,nsl), dtype=np.float64)
        LS_matel['V1%i'%i] = lil_matrix((nsl,nsl), dtype=np.float64)

    # U(k) parameters
    line = f.readline()
    while (True):
        line = f.readline()
        if 'END' in line:
            break
        line = line.split()
        ii = LS_terms.index(line[0])
        jj = LS_terms.index(line[1])
        for i in range(7):
            LS_matel['U%i'%i][ii, jj] = line[i+2]

    # V(1k)
    line = f.readline()
    while (True):
        line = f.readline()
        if 'END' in line:
            break
        line = line.split()
        ii = LS_terms.index(line[0])
        jj = LS_terms.index(line[1])
        for i in range(7):
            LS_matel['V1%i'%i][ii, jj] = line[i+2]
   
    # Electrostatic; skip for single electron/hole
    if ((nelectrons > 1) and (nelectrons < 13)):
        line = f.readline()
        tensors = ['F2', 'F4', 'F6', 'ALPHA', 'BETA', 'GAMMA']
        if (nelectrons != 2):
            # No tk for f2
            tensors += ['T2', 'T3', 'T4', 'T6', 'T7', 'T8']
        for t in tensors:
            LS_matel[t] = lil_matrix((nsl,nsl), dtype=np.float64)
        
        # Set of 6 columns are diag elements of Slater and Racah params
        for ii in range(nsl):
            line = f.readline()
            line = line.split()
            for i,t in enumerate(tensors[:6]):
                LS_matel[t][ii, ii] = line[i+1]
        # Next set of 6 columns are tk, if present
        if (nelectrons != 2):
            for ii in range(nsl):
                line = f.readline()
                line = line.split()
                for i,t in enumerate(tensors[6:]):
                    LS_matel[t][ii, ii] = line[i+1]
            
        # Now we get off-diag terms. These as (value, index) pairs, with the
        # following format: first 1 or 2 elements denotes tensor number, next
        # three elements are row number, and last three are col number. 
        while (True):
            line = f.readline()
            if 'END' in line:
                break
            line = line.split()
            for i in range(len(line)/2):
                mel = line[i*2]
                idx = line[i*2+1]
                # Convert from 1 based index
                ti = int(idx[:-6])-1
                ii = int(idx[-6:-3])-1
                jj = int(idx[-3:])-1
                LS_matel[tensors[ti]][ii, jj] = mel
                      
        # Mk and Pk; also not present for f1/f13
        tensors = ['M0', 'M2', 'M4', 'P2', 'P4', 'P6']
        for t in tensors:
            LS_matel[t] = lil_matrix((nsl,nsl), dtype=np.float64)

        line = f.readline()
        while (True):
            line = f.readline()
            if 'END' in line:
                break
            line = line.split()
            # Some lines for Mk/Pk run columns together... assume it's always
            # the fault of a -sign, so manually add space to -signs and re-split
            if len(line) != 8:
                line = sum([l.replace('-', ' -').split() for l in line], [])
            
            ii = LS_terms.index(line[0])
            jj = LS_terms.index(line[1])
            for i,t in enumerate(tensors):
                LS_matel[t][ii, jj] = line[i+2]
    
    # Scale Crosswhite alpha by 1000, following Mike's code. 
    LS_matel['ALPHA'] = 1000*LS_matel['ALPHA']

    if nelectrons > 1:
        LS_matel['ZETA'] = np.sqrt(3*(3+1)*(2*3+1))*LS_matel['V11']

    ls_l = [SL_label(t) for t in LS_terms]
    
    return (LS_matel, ls_l)    
   


def tkk0val(k, rme, s1, l1, j1, s2, l2, j2):
    """Use Wigner-Eckart theorem to calculate SLJ singly reduced matrix elements
    of a tensor k, k, 0, from SL doubly reduced matrix elements (eg., magnetic
    interactions -- ZETA, Pk, Mk -- S=1, L=1, J=0).  Using Judd, Eqn. (3-36).
    
    NOTE: we differ here from Mike's implementation by a factor of sqrt(2*k+1),
    which, in readcr.p, he multiplies matrix elements of ZETA, Pk, Mk by and
    then divides out here for consistency with W110. See his comments in the
    readcr header from Nov 14 1992 for detals. 
    """ 
    if j1 != j2:
        tkk0val = 0
    else:
        if (j1+l1+s2) % 2:
            sign = -1
        else: 
            sign = 1
        tkk0val = sign*wigner_6j(l1,l2,k,s2,s1,j1)*rme
    
    return tkk0val

def t0kkval(k, rme, s1, l1, j1, s2, l2, j2):
    """Use Wigner-Eckart theorem to calculate SLJ singly reduced matrix elements
    of a tensor 0, k, k, from SL doubly reduced matrix elements (eg., Uk -- S=0,
    L=k, J=k).  Using Judd, Eqn. (3-38).""" 
    if s1 != s2:
        t0kkval = 0
    else:
        if (s2+l2+j1+k) % 2:
            sign = -1
        else: 
            sign = 1
        t0kkval = sign*np.sqrt((2*j1+1)*(2*j2+1))*wigner_6j(j1,k,j2,l2,s1,l1)*rme

    return t0kkval

def tk0val(k, rme, s1, l1, j1, s2, l2, j2):
    """Use Wigner-Eckart theorem to calculate SLJ singly reduced matrix elements
    of a tensor k, 0, k, from SL doubly reduced matrix elements.  Using Judd,
    Eqn. (3-37).""" 
    
    if l1 != l2:
        tk0val = 0
    else:
        if (s1+l1+j2+k) % 2:
            sign = -1
        else:
            sign = 1
        tk0val = sign*np.sqrt((2*j1+1)*(2*j2+1))*wigner_6j(j1,k,j2,s2,l1,s1)*rme

    return tk0val



def sljcalc(LS_matel, sl_l):
    """
    Directly following Mike's sljcalc implementation except that it's more
    limited in scope and doesn't handle tk0k tensors.  

    Parameters
    ----------
    LS_matel : dict
        Dictonary of matrix elements for tensors. 
    sl_l : list
        List of SL coupled state labels.
    """
    tslj_lookup = {
            # Elements are S, L, J (not doubled)
            'F2'    : [0, 0, 0], 
            'F4'    : [0, 0, 0], 
            'F6'    : [0, 0, 0], 
            'ALPHA' : [0, 0, 0],
            'BETA'  : [0, 0, 0], 
            'GAMMA' : [0, 0, 0],
            'T2'    : [0, 0, 0],
            'T3'    : [0, 0, 0],
            'T4'    : [0, 0, 0],
            'T6'    : [0, 0, 0],
            'T7'    : [0, 0, 0],
            'T8'    : [0, 0, 0],
            'ZETA'  : [1, 1, 0],
            'M0'    : [1, 1, 0],
            'M2'    : [1, 1, 0],
            'M4'    : [1, 1, 0],
            'P2'    : [1, 1, 0],
            'P4'    : [1, 1, 0],
            'P6'    : [1, 1, 0],
            'U0'    : [0, 1, 1],
            'U1'    : [0, 1, 1],
            'U2'    : [0, 2, 2],
            'U3'    : [0, 3, 3],
            'U4'    : [0, 4, 4],
            'U5'    : [0, 5, 5],
            'U6'    : [0, 6, 6]}

    tensor_list = [t for t in LS_matel if t not in ['V12', 'V13', 'V10', 'V11',
        'V16', 'V14', 'V15']]
    
    nslj = 0
    slj_l = []
    for i,l in enumerate(sl_l):
        jmin = np.abs(l.S-l.L)
        jmax = l.S + l.L + 1
        
        for j in range(jmin, jmax, 2):
            slj_l += [SLJ_label(l, j, i)]
            nslj += 1
     
    LSJ_matel = {}
    #FIXME: add eavg
    for t in tensor_list:
        LSJ_matel[t] = lil_matrix((nslj, nslj), dtype=np.float64)
        
        # SL scalar
        if tslj_lookup[t] == [0, 0, 0]:
            for i in range(nslj):
                for k in range(i, nslj):
                    if slj_l[i].label == slj_l[k].label:
                        sli = slj_l[i].whichSL            
                        rme = LS_matel[t][sli, sli]
                        if rme != 0:
                            LSJ_matel[t][i, k] = rme

        # Magnetic interactions
        elif tslj_lookup[t] == [1, 1, 0]:
            for i in range(nslj):
                for k in range(i, nslj):
                    sli = slj_l[i].whichSL            
                    slk = slj_l[k].whichSL
                    rme = LS_matel[t][sli, slk]

                    # Divide by 2, since I'm following Mike and storing twice
                    # the S, L, and J, but my tkk0val expects the actual value.
                    if rme != 0:
                        mel = tkk0val(1, rme, 
                            slj_l[i].S/2, slj_l[i].L/2, slj_l[i].J/2,
                            slj_l[k].S/2, slj_l[k].L/2, slj_l[k].J/2)

                        if mel != 0:
                            LSJ_matel[t][i, k] = mel
       
        # Uk
        elif tslj_lookup[t][0] == 0:
            for i in range(nslj):
                for k in range(i, nslj):
                    sli = slj_l[i].whichSL            
                    slk = slj_l[k].whichSL
                    rme = LS_matel[t][sli, slk]
                    
                    if rme != 0:
                        mel = t0kkval(tslj_lookup[t][1], rme,
                                slj_l[i].S/2, slj_l[i].L/2, slj_l[i].J/2,
                                slj_l[k].S/2, slj_l[k].L/2, slj_l[k].J/2)
                        if mel != 0:
                            LSJ_matel[t][i, k] = mel
                
    return (LSJ_matel, slj_l)    
